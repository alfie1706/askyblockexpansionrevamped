package me.alfie.expansions;

public class Placeholders {

    public static final String CURRENT_ISLAND = "at";
    public static final String ISLAND_TOP = "top";
    public static final String ADMIN = "admin";
    public static final String ABOUT = "about";

    // Personal Island Placeholders
        // Misc
        public static final String HAS_ISLAND = "has_island";
        public static final String HAS_NO_ISLAND = "has_no_island";
        public static final String IS_OWNER = "is_owner";
        public static final String HAS_TEAM = "has_team";

        // Stats
        public static final String ISLAND_LEVEL = "level";
        public static final String ISLAND_LEVEL_FORMATTED = "level-formatted";
        public static final String ISLAND_LOCKED = "locked";
        public static final String ISLAND_OWNER = "owner";
        public static final String ISLAND_NAME = "name";
        public static final String ISLAND_TEAM_SIZE = "team_size";
        public static final String ISLAND_COOP = "coop_islands";

        // Members
        public static final String ISLAND_TOTALMEMBERS = "members_total";
        public static final String ISLAND_ONLINE = "members_online";
        public static final String ISLAND_OFFLINE = "members_offline";
        public static final String ISLAND_MEMBER = "member"; // This is to grab a specific island member
        public static final String ISLAND_MAX = "members_max";

        // Location
        public static final String ISLAND_X = "island_x";
        public static final String ISLAND_Y = "island_y";
        public static final String ISLAND_Z = "island_z";

    // Island Top
        public static final String ISLAND_TOP_LEVEL = "top_level";
        public static final String ISLAND_TOP_LEADER = "top_leader";
        public static final String ISLAND_TOP_MEMBERCOUNT = "top_membercount";
        public static final String ISLAND_TOP_NAME = "top_name";


    // Island visiting placeholders
        // Misc
        public static final String CURRENT_ISLAND_EXISTS = "at_exists";
        public static final String CURRENT_ISLAND_ISOWNER = "at_isowner";
        public static final String CURRENT_ISLAND_PVP = "at_pvp";

        // Stats
        public static final String CURRENT_ISLAND_LEVEL = "at_level";
        public static final String CURRENT_ISLAND_LEVEL_FORMATTED = "at_level-formatted";
        public static final String CURRENT_ISLAND_LOCKED = "at_locked";
        public static final String CURRENT_ISLAND_OWNER = "at_owner";
        public static final String CURRENT_ISLAND_NAME = "at_name";
        public static final String CURRENT_ISLAND_TEAM_SIZE = "at_team_size";
        public static final String CURRENT_ISLAND_COOP = "at_coop_islands";

        // Members
        public static final String CURRENT_ISLAND_ONLINE = "at_members_online";
        public static final String CURRENT_ISLAND_OFFLINE = "at_members_offline";
        public static final String CURRENT_ISLAND_TOTALMEMBERS = "at_total_members";

        // Location
        public static final String CURRENT_ISLAND_X = "at_island_x";
        public static final String CURRENT_ISLAND_Y = "at_island_y";
        public static final String CURRENT_ISLAND_Z = "at_island_z";


    // Admin placeholders
        public static final String ADMIN_ISLAND_COUNT = "admin_island-count";


    // Misc Placeholders
        public static final String COMBAT_SCOREBOARD = "combat_scoreboard";
}
