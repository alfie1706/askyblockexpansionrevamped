package me.alfie.expansions;

import com.wasteofplastic.askyblock.*;
import com.wasteofplastic.askyblock.Island.SettingsFlag;
import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachmentInfo;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.bukkit.Bukkit.getOfflinePlayer;

public class SkyblockExpansion extends PlaceholderExpansion {
    private final DecimalFormat HIGH_LEVEL_FORMATTER = new DecimalFormat("#,###.00");
    private final DecimalFormat LEVEL_FORMATTER = new DecimalFormat("#,###");

    // Defining things
    public final String AUTHOR = "Alfie";
    public final String VERSION = "1.0";
    public final String IDENTIFIER = "askyblock";
    public final String DEPENDS = "ASkyBlock";

    @Override
    public boolean canRegister() {
        return Bukkit.getPluginManager().getPlugin(getPlugin()) != null;
    }

    @Override
    public String getAuthor() {
        return AUTHOR;
    }

    @Override
    public String getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getPlugin() {
        return DEPENDS;
    }

    @Override
    public String getVersion() {
        return VERSION;
    }

    private final String pvpDisabledString = ChatColor.RED + "Disabled";
    private final String pvpEnabledString = ChatColor.GREEN + "Enabled";


    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        try {
            if (identifier == null) {
                return null;
            }
            String[] part = identifier.split("_");

            if (identifier.equals(Placeholders.ABOUT))
                return String.join("", new String[] {
                        "-------- START DEBUG --------\n",
                        "ASkyblockExpansionRevamped v" + VERSION + " by " + AUTHOR + "\n",
                        "Identifier: " + IDENTIFIER + "\n",
                        "Server Version: " + Bukkit.getServer().getVersion(),
                        "Hook: " + Bukkit.getServer().getPluginManager().getPlugin("ASkyBlock").getName(),
                        "\n-------- END OF DEBUG --------"
                });

            checkIsland(player);
            Island playerIsland = getIsland(player);
            Location playerLocation = player.getLocation();

            // Misc
            if (identifier.equals(Placeholders.HAS_ISLAND))
                return this.parseHasIsland(player, false);

            if (identifier.equals(Placeholders.HAS_NO_ISLAND))
                return this.parseHasIsland(player, true);

            if (identifier.equals(Placeholders.IS_OWNER))
                return playerIsland.getOwner() == player.getUniqueId() ? "true" : "false";

            if (identifier.equals(Placeholders.HAS_TEAM))
                return hasTeam(player);

            // Stats
            if (identifier.equals(Placeholders.ISLAND_NAME))
                return getIslandName(playerIsland);

            if (identifier.equals(Placeholders.ISLAND_LEVEL))
                return getIslandLevel(playerIsland);

            if (identifier.equals(Placeholders.ISLAND_LEVEL_FORMATTED))
                return getFormattedIslandLevel(playerIsland);

            if (identifier.equals(Placeholders.ISLAND_LOCKED))
                return getLockedStatus(playerIsland);

            if (identifier.equals(Placeholders.ISLAND_OWNER))
                return getOwner(playerIsland);

            if (identifier.equals(Placeholders.ISLAND_COOP))
                return getCoopIslands(playerIsland);

            if (identifier.equals(Placeholders.CURRENT_ISLAND_TEAM_SIZE))
                return getTeamSize(playerIsland);

            if (identifier.equals(Placeholders.ISLAND_MAX))
                return maxSize(player);

            // Members
            if (identifier.startsWith(Placeholders.ISLAND_MEMBER))
                getUsername(playerIsland.getMembers().get(Integer.parseInt(part[3])));

            if (identifier.equals(Placeholders.ISLAND_ONLINE))
                return getOnlineUsers(playerIsland);

            if (identifier.equals(Placeholders.ISLAND_OFFLINE))
                return getOfflineUsers(playerIsland);

            if (identifier.equals(Placeholders.ISLAND_TOTALMEMBERS))
                return getString(getIslandMembersCorrect(playerIsland).size());

            // Location
            if(identifier.equals(Placeholders.ISLAND_X))
                return String.valueOf(ASkyBlock.getPlugin().getPlayers().getIslandLocation(player.getUniqueId()).getBlockX());

            if(identifier.equals(Placeholders.ISLAND_Y))
                return String.valueOf(ASkyBlock.getPlugin().getPlayers().getIslandLocation(player.getUniqueId()).getBlockY());

            if(identifier.equals(Placeholders.ISLAND_Z))
                return String.valueOf(ASkyBlock.getPlugin().getPlayers().getIslandLocation(player.getUniqueId()).getBlockZ());


            // IS Top
            if (identifier.startsWith(Placeholders.ISLAND_TOP)) {
                ASkyBlockAPI asb = ASkyBlockAPI.getInstance();

                if(part.length >= 4) {
                    int pri = Integer.parseInt(part[3]);
                    final String[] sortedOutput = sortByValue(asb.getLongTopTen()).entrySet().toArray()[pri-1].toString().split("=");
                    final UUID outputUUID = UUID.fromString(sortedOutput[0]);

                    if (identifier.startsWith(Placeholders.ISLAND_TOP_NAME))
                        return asb.getIslandName(outputUUID);

                    if (identifier.startsWith(Placeholders.ISLAND_TOP_LEVEL))
                        return sortedOutput[1];

                    if (identifier.startsWith(Placeholders.ISLAND_TOP_LEADER))
                        return getOfflinePlayer(outputUUID).getName();

                    if (identifier.startsWith(Placeholders.ISLAND_TOP_MEMBERCOUNT))
                        return String.valueOf(asb.getIslandOwnedBy(outputUUID).getMembers().size());
                }

                return "";
            }


            // Visiting Placeholders
            if (identifier.startsWith(Placeholders.CURRENT_ISLAND)) {
                final Island islandAt = ASkyBlockAPI.getInstance().getIslandAt(playerLocation);

                if (identifier.equals(Placeholders.CURRENT_ISLAND_EXISTS))
                    return islandAt != null ? "true" : "false";

                if (identifier.equals(Placeholders.CURRENT_ISLAND_OWNER))
                    return getOwner(islandAt);

                if (identifier.equals(Placeholders.CURRENT_ISLAND_LOCKED))
                    return String.valueOf(islandAt.isLocked());

                if (identifier.equals(Placeholders.CURRENT_ISLAND_ISOWNER))
                    return player.getName() == getOwner(islandAt) ? "true" : "false";

                if (identifier.equals(Placeholders.CURRENT_ISLAND_ONLINE))
                    return getOnlineUsers(islandAt);

                if (identifier.equals(Placeholders.CURRENT_ISLAND_NAME))
                    return getIslandName(playerIsland);

                if (identifier.equals(Placeholders.CURRENT_ISLAND_OFFLINE))
                    return getOfflineUsers(islandAt);

                if (identifier.equals(Placeholders.CURRENT_ISLAND_TOTALMEMBERS))
                    return getString(getIslandMembersCorrect(islandAt).size());

                if (identifier.equals(Placeholders.CURRENT_ISLAND_LEVEL))
                    return getLevel(islandAt, false);

                if (identifier.equals(Placeholders.CURRENT_ISLAND_LEVEL_FORMATTED))
                    return getLevel(islandAt, true);

                if (identifier.equals(Placeholders.CURRENT_ISLAND_PVP))
                    return this.getIslandPvpString(islandAt);

                if (identifier.equals(Placeholders.CURRENT_ISLAND_TEAM_SIZE))
                    return getTeamSize(islandAt);

                if (identifier.equals(Placeholders.CURRENT_ISLAND_COOP))
                    return getCoopIslands(islandAt);

                // Location
                if(identifier.equals(Placeholders.CURRENT_ISLAND_X))
                    return String.valueOf(ASkyBlock.getPlugin().getPlayers().getIslandLocation(islandAt.getOwner()).getBlockX());

                if(identifier.equals(Placeholders.CURRENT_ISLAND_Y))
                    return String.valueOf(ASkyBlock.getPlugin().getPlayers().getIslandLocation(islandAt.getOwner()).getBlockY());

                if(identifier.equals(Placeholders.CURRENT_ISLAND_Z))
                    return String.valueOf(ASkyBlock.getPlugin().getPlayers().getIslandLocation(islandAt.getOwner()).getBlockZ());

                return null;
            }


            // Admin Placeholders
            if (identifier.startsWith(Placeholders.ADMIN)) {
                if(identifier.equals(Placeholders.ADMIN_ISLAND_COUNT))
                    return String.valueOf(ASkyBlockAPI.getInstance().getIslandCount());
            }

            // Misc
            if (identifier.equals(Placeholders.COMBAT_SCOREBOARD))
                return this.parseCombadScoreboard(player);

            return null;

        } catch (Exception e) {
            return null;
        }
    }


    private Island getIsland(Player player) {
        UUID owner = ASkyBlockAPI.getInstance().getTeamLeader(player.getUniqueId());
        return ASkyBlockAPI.getInstance().getIslandOwnedBy(owner);
    }

    private String parseHasIsland(Player player, boolean negate) {
        final boolean hasIsand = ASkyBlockAPI.getInstance().hasIsland(player.getUniqueId());
        final String trueString = negate ? "false" : "true";
        final String falseString = negate ? "true" : "false";

        return hasIsand ? trueString : falseString;
    }

    private String getOfflineUsers(Island island) {
        int count = 0;
        for(UUID uuid : getIslandMembersCorrect(island)) {
            if(Bukkit.getPlayer(uuid) == null) {
                count++;
            }
        }

        return String.valueOf(count);
    }

    private String getOnlineUsers(Island island) {
        int count = 0;
        for(UUID uuid : getIslandMembersCorrect(island)) {
            if(Bukkit.getPlayer(uuid) != null)
                count++;
        }

        return String.valueOf(count);
    }

    private String getLevel(Island island, boolean isFormatted) {
        if(isFormatted)
            return this.formatIslandLevel(ASkyBlockAPI.getInstance().getLongIslandLevel(island.getOwner()));
        else
            return String.valueOf(ASkyBlockAPI.getInstance().getLongIslandLevel(island.getOwner()));
    }

    private String parseCombadScoreboard(Player player) {
        final String remaining = PlaceholderAPI.setPlaceholders(player, "%combatlogx_time_left%");
        return "0".equalsIgnoreCase(remaining) ? "None" : remaining + "s";
    }

    private String getIslandOwnerName(Island island) {
        final Players players = ASkyBlock.getPlugin().getPlayers().get(island.getOwner());
        return players == null ? null : players.getPlayerName();
    }

    private UUID getIslandOwnerUUID(Island island) {
        final Players players = ASkyBlock.getPlugin().getPlayers().get(island.getOwner());
        return players == null ? null : players.getPlayerUUID();
    }

    private String getFormattedIslandLevel(Island island) {
        final long level = Long.parseLong(this.getIslandLevel(island));
        return this.formatIslandLevel(level);
    }

    private String getIslandLevel(Island island) {
        return String.valueOf(ASkyBlockAPI.getInstance().getLongIslandLevel(island.getOwner()));
    }

    private String getIslandPvpString(Island island) {
        if (island == null) {
            return this.pvpDisabledString;
        }

        final boolean value = island.getIgsFlag(SettingsFlag.PVP);
        return value ? this.pvpEnabledString : this.pvpDisabledString;
    }

    private String formatIslandLevel(long level) {
        if (level < 1000) {
            return LEVEL_FORMATTER.format(level);
        }
        final double levelsByThousand = level / 1000D;
        return HIGH_LEVEL_FORMATTER.format(levelsByThousand) + "k";
    }

    public static Map<UUID, Long> sortByValue(final Map<UUID, Long> map) {

        return map.entrySet()
                .stream()
                .sorted((Map.Entry.<UUID, Long>comparingByValue().reversed()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    public List<UUID> getIslandMembersCorrect (Island island) {
        List<UUID> outputMembers = island.getMembers();
        outputMembers.clear();

        for(UUID uuid : island.getMembers()) {
            if(!outputMembers.contains(uuid))
                outputMembers.add(uuid);
        }

        return outputMembers;
    }

    public String getString(Object obj) {
        return String.valueOf(obj);
    }

    public String getOwner(Island island) {
        return getIslandOwnerName(island);
    }

    public String getLockedStatus(Island island) {
        return String.valueOf(island.isLocked());
    }

    public String getUsername(UUID uuid) {
        return Bukkit.getPlayer(uuid).getName();
    }

    public void checkIsland(Player player) {
        if(!ASkyBlockAPI.getInstance().playerIsOnIsland(player) && ASkyBlockAPI.getInstance().getIslandAt(player.getLocation()) == null)
            return;
    }

    public String getIslandName(Island island) {
        return ASkyBlockAPI.getInstance().getIslandName(getIslandOwnerUUID(island));
    }

    public String getCoopIslands(Island island) {
        final Set<Location> coops = CoopPlay.getInstance().getCoopIslands(Bukkit.getPlayer(island.getOwner()));
        return coops != null ? String.valueOf(coops.size()) : "0";
    }

    public String hasTeam(Player player) {
        return ASkyBlock.getPlugin().getPlayers().inTeam(player.getUniqueId()) ? "true" : "false";
    }

    public String maxSize(Player player) {
        if(!player.hasPermission("askyblock.team.maxsize.*")) {
            for (PermissionAttachmentInfo perms : player.getEffectivePermissions()) {
                if(perms.getPermission().startsWith("askyblock.team.maxsize.")) {
                    return perms.getPermission().split("askyblock.team.maxsize.")[1];
                }
            }
        }

        return "0";
    }

    public String getTeamSize(Island island) {
        List<UUID> members = ASkyBlock.getPlugin().getPlayers().getMembers(island.getOwner());
        return members != null ? String.valueOf(members.size()) : "0";
    }
}
